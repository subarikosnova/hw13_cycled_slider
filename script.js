const images = document.querySelectorAll('.image-to-show');
let currIndex = 0;
let lineId;

function showImage(index) {
    images.forEach(image => image.style.display = 'none');
    images[index].style.display = 'block';
}

function stopSlide() {
    clearInterval(lineId);
}

function startSlide() {
    showImage(currIndex);
    lineId = setInterval(() => {
        currIndex = (currIndex + 1) % images.length;
        showImage(currIndex);
    }, 3000);
}

function resumeSlide() {
    startSlide();
}

const startBtn = document.getElementById('startButton');
const stopBtn = document.getElementById('stopButton');
const resumeBtn = document.getElementById('resumeButton');

startBtn.addEventListener('click', () => {
    startSlide();
});
stopBtn.addEventListener('click', () => {
    stopSlide();
});
resumeBtn.addEventListener('click', () => {
    resumeSlide();
});